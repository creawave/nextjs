import React, { useEffect } from "react";
import Head from 'next/head';
import { useDispatch, useSelector } from "react-redux";
import { TPost } from "../store/reducers/posts";
import { getPostsAction } from "../store/actions/posts";
import {
    Card,
    Container,
    Descriptions,
    Section, Title,
    Wrapper,
    WrapperBox,
} from "../styles";
import Link from "next/link";

const Index:React.FC = () => {
    const dispatch = useDispatch();
    interface RootState {
        posts: TPost,
        loading: boolean,
    }
    const typesPosts = useSelector((state:RootState) => state.posts.posts);
    const loading = useSelector((state:RootState) => state.posts.loading);

    useEffect(() => {
        dispatch(getPostsAction());
    }, []);
    if (loading) {
        return <div>Loading...</div>
    }
    return (
        <div>

            <Head>
                <title>Create Next App</title>
            </Head>
            <Section>
                <Container>
                    <Wrapper>
                        {
                            typesPosts.filter(({body, title}) => Boolean(body) && Boolean(title)).map(({ id, body, title }) => {
                                return (
                                    <WrapperBox key={id}>
                                        <Link href={`/posts/${id}`}>
                                            <Card>
                                                <Title>
                                                    {title}
                                                </Title>
                                                <Descriptions>
                                                    {body}
                                                </Descriptions>
                                            </Card>
                                        </Link>
                                    </WrapperBox>
                                )
                            })
                        }
                    </Wrapper>
                </Container>
            </Section>
        </div>
    )
};

export default Index;