import React, {useState} from "react";
import {
    Box,
    Button,
    Container,
    Input,
    Section,
    Textarea,
    Wrapper
} from "../../styles";
import { useDispatch } from "react-redux";
import {createPostAction} from "../../store/actions/posts";

const New = () => {
    const [input, setInput] = useState('');
    const [textarea, setTextarea] = useState('');
    const dispatch = useDispatch();
    return (
        <Section>
            <Container>
                <Wrapper>
                    <Box>
                        <Input type='text' value={input} onChange={(e) => setInput(e.currentTarget.value)}/>
                        <Textarea type='text' value={textarea} onChange={(e) => setTextarea(e.currentTarget.value)}/>
                        <Button onClick={() => {
                            setInput('');
                            setTextarea('');
                            dispatch(createPostAction({title: input, body: textarea}));
                        }}>Submit</Button>
                    </Box>
                </Wrapper>
            </Container>
        </Section>
    )
};

export default New;