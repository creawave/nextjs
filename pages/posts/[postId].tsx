import React, {useEffect} from "react";
import { useRouter } from "next/router";
import { TPost } from "../../store/reducers/posts";
import { useDispatch, useSelector } from "react-redux";
import {
    Container,
    Section,
    Title,
    Wrapper,
    PostCard,
    Descriptions,
} from "../../styles";
import { getPostAction } from "../../store/actions/posts";

const Post = () => {
    interface RootState {
        posts: TPost,
        post: TPost,
        loading: boolean,
    }
    const router = useRouter().query;
    const { postId } = router;
    const loading = useSelector((state:RootState) => state.posts.loading);
    const post = useSelector((state:RootState) => state.posts.post);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPostAction(postId))
    }, []);
    if (loading) {
        return <div>Loading...</div>
    }
    return (
        <Section>
            <Container>
                <Wrapper>
                    <PostCard>
                        <Title>
                            {post?.title}
                        </Title>
                        <Descriptions>
                            {post?.body}
                        </Descriptions>
                    </PostCard>
                </Wrapper>
            </Container>
        </Section>
    )
};

export default Post;