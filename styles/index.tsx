import styled from 'styled-components';

export const Section = styled.section`
    max-width: 1180px;
    margin: 10px auto;
    width: 100%;
`;

export const Container = styled.div`
    margin-left: -15px;
`;

export const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const WrapperBox = styled.div`
    padding: 15px;
    width: 33.3333%;
    cursor: pointer;
`;

export const Card = styled.div`
    padding: 20px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    height: 100%;
    
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;

export const Button = styled.button`
    background: linear-gradient(#a8d044,#95bb38 60%,#95bb38 90%,#94b83a);
    box-shadow: inset 0 1px 0 hsla(0,0%,100%,.1);
    color: white;
    padding: 0 14px;
    height: 37px;
    font-size: 13px;
    line-height: 37px;
    font-weight: 400;
    text-align: center;
    border-radius: 4px;
    border: 1px solid transparent;
    cursor: pointer;
    
    &:focus {
        outline: none;
        border: 1px solid black;
    }
`;

export const Title = styled.h1`
    font-size: 22px;
    line-height: 1.15em;
    margin-bottom: 10px;
`;

export const Box = styled.div`
    max-width: 600px;
    margin: 0 auto;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const Input = styled.input`
    width: 100%;
    padding: 12px;
    border: 1px solid #dbe3e7;
    color: #677d87;
    font-size: 16px;
    font-weight: 400;
    border-radius: 5px;
    margin-bottom: 20px;
`;

export const Textarea = styled.textarea`
    width: 100%;
    resize: none;
    padding: 12px;
    border: 1px solid #dbe3e7;
    color: #677d87;
    font-size: 16px;
    font-weight: 400;
    border-radius: 5px;
    margin-bottom: 20px;
`;

export const Home = styled.p`
    font-size: 16px;
    font-weight: 400;
    border: 1px solid green;
    background: transparent;
    color: green;
    border-radius: 4px;
    cursor: pointer;
    padding: 5px 14px;
`;

export const Menu = styled.div`
    max-width: 1180px;
    margin: 10px auto;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const Descriptions = styled.div`
    color: #738a94;
`;

export const PostCard = styled.div`
    max-width: 800px;
    margin: 10px auto;
    width: 100%;
    text-align: center;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    padding: 20px;
    
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;
