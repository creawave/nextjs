import React from "react";
import Link from "next/link";
import {
    Button,
    Home,
    Menu
} from "../styles";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Layout:React.FC = ({children}) => {
    return (
        <div>
            <Menu>
                <Link href='/posts/new'>
                    <Button>Create Post</Button>
                </Link>
                <Link href='/'>
                   <Home>
                       Home
                   </Home>
                </Link>
            </Menu>
            <ToastContainer position='top-right'/>
            {children}
        </div>
    )
};

export default Layout;