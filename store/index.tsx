import React from "react";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import {posts} from "./reducers/posts";

const rootReducers = combineReducers({
    posts: posts,
});

const middleware = [thunk];
const middlewareEnhancer = applyMiddleware(...middleware);

const store = createStore(rootReducers, composeWithDevTools(middlewareEnhancer));



export default store;
export type RootState = ReturnType<typeof rootReducers>