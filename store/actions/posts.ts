import axios from 'axios';
import { toast } from "react-toastify";
import {
    GetPost,
    GetPosts,
    GetPostsSuccess,
    GetPostSuccess,
    ObjPost
} from '../interface/posts';
import {
    GET_POSTS_ERROR,
    GET_POSTS,
    GET_POSTS_SUCCESS,
    GET_POST,
    GET_POST_SUCCESS,
    GET_POST_ERROR,
    CREATE_POST,
    CREATE_POST_SUCCESS,
    CREATE_POST_ERROR
} from "../actionsTypes/posts";


export function getPostsAction() {
    return (dispatch:any, getState:any) => {
        dispatch(getPosts());
        axios.get(`https://simple-blog-api.crew.red/posts`)
            .then(res => {
                dispatch(getPostsSuccess(res.data));
            })
            .catch(err => dispatch({type: GET_POSTS_ERROR}))
    }
}

const getPosts = ():GetPosts => ({
    type: GET_POSTS,
    loading: true,
});
const getPostsSuccess = (posts: ObjPost[]):GetPostsSuccess => ({
    type: GET_POSTS_SUCCESS,
    payload: posts,
});

export function getPostAction(id) {
    return (dispatch:any, getState:any) => {
        dispatch(getPost());
        axios.get(`https://simple-blog-api.crew.red/posts/${id}`)
            .then(res => {
                dispatch(getPostSuccess(res.data));
            })
            .catch(err => dispatch({type: GET_POST_ERROR}))
    }
}

const getPost = ():GetPost => ({
    type: GET_POST,
    loading: true,
});
const getPostSuccess = (post: ObjPost):GetPostSuccess => ({
    type: GET_POST_SUCCESS,
    payload: post,
});

export function createPostAction(data) {
    return (dispatch:any, getState:any) => {
        dispatch({type: CREATE_POST});
        axios.post(`https://simple-blog-api.crew.red/posts`, data)
            .then(res => {
                dispatch({type: CREATE_POST_SUCCESS});
                toast.success('Success');
            })
            .catch(err => dispatch({type: CREATE_POST_ERROR}))
    }
}

