import React from "react";
import {
    GET_POSTS_ERROR,
    GET_POSTS,
    GET_POSTS_SUCCESS,
    GET_POST,
    GET_POST_SUCCESS,
    GET_POST_ERROR,
} from "../actionsTypes/posts";

export interface ObjPost {
    id?: number,
    body: string,
    title: string,
}

export type GetPosts = {
    type: typeof GET_POSTS;
    loading: boolean,
}

export type GetPostsSuccess = {
    type: typeof GET_POSTS_SUCCESS;
    payload: ObjPost[];
}

export type GetPostsError = {
    type: typeof GET_POSTS_ERROR;
}

export type GetPost = {
    type: typeof GET_POST;
    loading: boolean,
}

export type GetPostSuccess = {
    type: typeof GET_POST_SUCCESS;
    payload: ObjPost;
}

export type GetPostError = {
    type: typeof GET_POST_ERROR;
}

export type TAllActions = GetPosts |
    GetPostsSuccess |
    GetPostsError |
    GetPost |
    GetPostSuccess |
    GetPostError;
