import React from "react";
import {ObjPost, TAllActions} from "../interface/posts";
import {
    GET_POSTS_ERROR,
    GET_POSTS,
    GET_POSTS_SUCCESS,
    GET_POST,
    GET_POST_SUCCESS,
    GET_POST_ERROR, CREATE_POST
} from "../actionsTypes/posts";

export type StatePosts = ObjPost[];

export type TPost = {
    posts: StatePosts,
    post: ObjPost | null,
    loading: boolean,
}

const initialState:TPost = {
    posts: [],
    post: null,
    loading: false,
};

export const posts = (state = initialState, action:TAllActions):TPost => {
    switch (action.type) {
        case GET_POSTS:
            return {
                ...state,
                loading: true,
            };
        case GET_POSTS_SUCCESS:
            return {
                ...state,
                loading: false,
                posts: [...action.payload]
            };
        case GET_POSTS_ERROR:
            return {
                ...state,
                loading: false,
            };
        case GET_POST:
            return {
                ...state,
                loading: true,
            };
        case GET_POST_SUCCESS:
            return {
                ...state,
                loading: false,
                post: action.payload
            };
        case GET_POST_ERROR:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;
    }
};